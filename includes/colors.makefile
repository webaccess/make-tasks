#Regular
regclr=\033[0m
# Black - Regular
txtblk=\033[0;30m
# Red
txtred=\033[0;31m
# Green
txtgrn=\033[0;32m
# Yellow
txtylw=\033[0;33m
# Blue
txtblu=\033[0;34m
# Purple
txtpur=\033[0;35m
# Cyan
txtcyn=\033[0;36m
# White
txtwht=\033[0;37m
# Black - Bold
bldblk=\033[1;30m
# Red
bldred=\033[1;31m
# Green
bldgrn=\033[1;32m
# Yellow
bldylw=\033[1;33m
# Blue
bldblu=\033[1;34m
# Purple
bldpur=\033[1;35m
# Cyan
bldcyn=\033[1;36m
# White
bldwht=\033[1;37m
# Black - Underline
unkblk=\033[4;30m
# Red
undred=\033[4;31m
# Green
undgrn=\033[4;32m
# Yellow
undylw=\033[4;33m
# Blue
undblu=\033[4;34m
# Purple
undpur=\033[4;35m
# Cyan
undcyn=\033[4;36m
# White
undwht=\033[4;37m
# Black - Background
bakblk=\033[40m  
# Red
bakred=\033[41m  
# Green
bakgrn=\033[42m  
# Yellow
bakylw=\033[43m  
# Blue
bakblu=\033[44m  
# Purple
bakpur=\033[45m  
# Cyan
bakcyn=\033[46m  
# White
bakwht=\033[47m  
# Text Reset
txtrst=\033[0m   