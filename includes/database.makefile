save-database-from-test:
	@echo "$(txtblu)Exporting database from : $(TEST_HOST) $(regclr)"
	@ssh $(TEST_HOST) "mysqldump --host=localhost --user=$(TEST_MYSQL_LOGIN) --password=$(TEST_MYSQL_PASSWORD) $(APP)" > $(BASE_FOLDER)$(APP)/src/$(APP)-backup.sql
	@sleep 5
	chmod -R 777 $(BASE_FOLDER)$(APP)/src/$(APP)-backup.sql
	docker exec $(APP)_database_1 sh -c 'mysql --user=$(LOCAL_MYSQL_LOGIN) --password=$(LOCAL_MYSQL_PASSWORD) wordpress < /var/www/html/$(APP)-backup.sql'
	@rm $(BASE_FOLDER)$(APP)/src/$(APP)-backup.sql
	@echo "$(txtgrn)Database imported successfully from test ! $(regclr)"

save-database-from-prod:
	@echo "$(txtblu)Exporting database from : $(PROD_HOST) $(regclr)"
	@ssh $(PROD_HOST) "mysqldump --host=$(PROD_DATABASE_HOST) --user=$(PROD_MYSQL_LOGIN) --password=$(PROD_MYSQL_PASSWORD) $(PROD_DATABASE)" > $(BASE_FOLDER)$(APP)/src/$(APP)-backup.sql
	@sleep 2
	docker exec $(APP)_database_1 sh -c 'mysql --user=$(LOCAL_MYSQL_LOGIN) --password=$(LOCAL_MYSQL_PASSWORD) wordpress < /var/www/html/$(APP)-backup.sql'
	@rm $(BASE_FOLDER)$(APP)/src/$(APP)-backup.sql
	@echo "$(txtgrn)Database imported successfully from prod ! $(regclr)"

update-database-urls-from-test:
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)options SET option_value = replace(option_value, '$(TEST_SITE_URL)/$(APP)', '$(LOCAL_SITE_URL):$(PORT)') WHERE option_name = 'home' OR option_name = 'siteurl';"
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)posts SET guid = replace(guid, '$(TEST_SITE_URL)/$(APP)', '$(LOCAL_SITE_URL):$(PORT)');"
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)posts SET post_content = replace(post_content, '$(TEST_SITE_URL)/$(APP)', '$(LOCAL_SITE_URL):$(PORT)');"
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)postmeta SET meta_value = replace(meta_value, '$(TEST_SITE_URL)/$(APP)', '$(LOCAL_SITE_URL):$(PORT)');"

update-database-urls-from-prod:
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)options SET option_value = replace(option_value, '$(PROD_SITE_URL)', '$(LOCAL_SITE_URL):$(PORT)') WHERE option_name = 'home' OR option_name = 'siteurl';"
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)posts SET guid = replace(guid, '$(PROD_SITE_URL)', '$(LOCAL_SITE_URL):$(PORT)');"
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)posts SET post_content = replace(post_content, '$(PROD_SITE_URL)', '$(LOCAL_SITE_URL):$(PORT)');"
	docker exec $(APP)_database_1 mysql --host=localhost -u$(LOCAL_MYSQL_LOGIN) -p$(LOCAL_MYSQL_PASSWORD) -e "USE wordpress; UPDATE $(DATABASE_PREFIX)postmeta SET meta_value = replace(meta_value, '$(PROD_SITE_URL)', '$(LOCAL_SITE_URL):$(PORT)');"

update-database-prefix:
	@sed -i '' 's/wp_/$(DATABASE_PREFIX)/g' $(BASE_FOLDER)$(APP)/src/wp-config.php
