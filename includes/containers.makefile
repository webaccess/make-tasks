create-wordpress-containers:
	@echo "$(txtblu)Initializing containers ... $(regclr)"
	@docker pull wordpress
	@cd $(BASE_FOLDER)$(APP) && docker-compose up -d database wordpress
	@sleep 15
	@echo "$(txtgrn)Containers initialized successfully !$(regclr)"

delete-env:
	@cd $(BASE_FOLDER)$(APP) && docker-compose down
