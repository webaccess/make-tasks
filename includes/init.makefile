EXTRA_INCLUDES:=$(wildcard $(BASE_FOLDER)$(APP)/config.makefile)
ifneq ($(strip $(EXTRA_INCLUDES)),)
  include $(EXTRA_INCLUDES)
endif

init-dev:
	@make create-wordpress-containers APP=$(APP)
	@make clone-default-theme APP=$(APP)
	@make remove-default-themes APP=$(APP)
	@make remove-default-plugins APP=$(APP)
	@make create-ga-file APP=$(APP)
	@cd $(BASE_FOLDER)$(APP) && docker-compose up -d phpmyadmin livereload sass
	@echo "Your dev environment is ready :"
	@echo "$(txtylw)$(LOCAL_SITE_URL):$(PORT) $(regclr)"

init-dev-from-test:
	@make create-wordpress-containers APP=$(APP)
	@make clone-theme APP=$(APP)
	@make remove-default-themes APP=$(APP)
	@make remove-default-plugins APP=$(APP)
	@make download-plugins-from-test APP=$(APP)
	@make download-uploads-from-test APP=$(APP)
	@make create-ga-file APP=$(APP)
	@make download-wp-config-from-test APP=$(APP)
	@make save-database-from-test APP=$(APP)
	@make update-database-urls-from-test APP=$(APP) PORT=$(PORT)
	@cd $(BASE_FOLDER)$(APP) && docker-compose up -d phpmyadmin livereload sass
	@echo "Your dev environment is ready :"
	@echo "$(txtylw)$(LOCAL_SITE_URL):$(PORT) $(regclr)"

init-dev-from-prod:
	@make create-wordpress-containers APP=$(APP)
	@make clone-theme APP=$(APP)
	@make remove-default-themes APP=$(APP)
	@make remove-default-plugins APP=$(APP)
	@make download-plugins-from-prod APP=$(APP)
	@make download-uploads-from-prod APP=$(APP)
	@make create-ga-file APP=$(APP)
	@make download-wp-config-from-prod APP=$(APP)
	@make save-database-from-prod APP=$(APP)
	@make update-database-urls-from-prod APP=$(APP) PORT=$(PORT)
	@make update-database-prefix
	@cd $(BASE_FOLDER)$(APP) && docker-compose up -d phpmyadmin livereload sass
	@echo "Your dev environment is ready :"
	@echo "$(txtylw)$(LOCAL_SITE_URL):$(PORT) $(regclr)"
