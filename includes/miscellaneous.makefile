clone-theme:
	@echo "$(txtblu)Cloning the theme ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP)/src/wp-content/themes && git clone -b develop $(GIT_REPOSITORY_URL)$(APP).git
	@echo "$(txtgrn)Theme cloned successfully ! $(regclr)"

clone-default-theme:
	@echo "$(txtblu)Cloning the default theme ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP)/src/wp-content/themes && git clone $(GIT_REPOSITORY_URL)wordpresstheme.git && mv wordpresstheme $(APP) && cd $(APP) && rm -rf .git && find ./ -type f -exec sed -i '' -e 's/_base_/$(APP)/g' {} \;
	@echo "$(txtgrn)Default theme cloned successfully ! $(regclr)"

remove-default-themes:
	@echo "$(txtblu)Removing default themes ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP)/src/wp-content/themes && rm -rf twenty*
	@echo "$(txtgrn)Default themes removed successfully ! $(regclr)"

remove-default-plugins:
	@echo "$(txtblu)Removing default plugins ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP)/src/wp-content/plugins && rm -rf akismet && rm hello.php
	@echo "$(txtgrn)Default plugins removed successfully ! $(regclr)"

create-ga-file:
	@echo "$(txtblu)Creating ga.php file ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP)/src/wp-content/themes/$(APP)/views/includes && touch ga.php
	@echo "$(txtgrn)The ga.php file was created successfully ! $(regclr)"

download-plugins-from-test:
	@echo "$(txtblu)Compressing plugins ... $(regclr)"
	@ssh $(TEST_HOST) "cd /var/www/$(APP)/wp-content && tar -czf plugins.tar.gz plugins"
	@echo "$(txtblu)Downloading plugins ... $(regclr)"
	@scp -r $(TEST_HOST):/var/www/$(APP)/wp-content/plugins.tar.gz $(BASE_FOLDER)$(APP)/src/wp-content
	@ssh $(TEST_HOST) "cd /var/www/$(APP)/wp-content && rm plugins.tar.gz"
	docker exec $(APP)_wordpress_1 sh -c 'cd /var/www/html/wp-content && tar -xzf plugins.tar.gz plugins && rm plugins.tar.gz'
	@echo "$(txtgrn)Plugins downloaded successfully from test ! $(regclr)"

download-uploads-from-test:
	@echo "$(txtblu)Compressing uploads ... $(regclr)"
	@ssh $(TEST_HOST) "cd /var/www/$(APP)/wp-content && tar -czf uploads.tar.gz uploads"
	@echo "$(txtblu)Downloading uploads ... $(regclr)"
	@scp -r $(TEST_HOST):/var/www/$(APP)/wp-content/uploads.tar.gz $(BASE_FOLDER)$(APP)/src/wp-content
	@ssh $(TEST_HOST) "cd /var/www/$(APP)/wp-content && rm uploads.tar.gz"
	docker exec $(APP)_wordpress_1 sh -c 'cd /var/www/html/wp-content && tar -xzf uploads.tar.gz uploads && rm uploads.tar.gz'
	@echo "$(txtgrn)Uploads downloaded successfully from test ! $(regclr)"

download-plugins-from-prod:
	@echo "$(txtblu)Compressing plugins ... $(regclr)"
	@ssh $(PROD_HOST) "cd $(PROD_ROOT_FOLDER)wp-content && tar -czf plugins.tar.gz plugins"
	@echo "$(txtblu)Downloading plugins ... $(regclr)"
	@scp -r $(PROD_HOST):$(PROD_ROOT_FOLDER)wp-content/plugins.tar.gz $(BASE_FOLDER)$(APP)/src/wp-content
	@ssh $(PROD_HOST) "cd $(PROD_ROOT_FOLDER)wp-content && rm plugins.tar.gz"
	docker exec $(APP)_wordpress_1 sh -c 'cd /var/www/html/wp-content && tar -xzf plugins.tar.gz plugins && sleep 3 && rm plugins.tar.gz'
	@echo "$(txtgrn)Plugins downloaded successfully from prod ! $(regclr)"

download-uploads-from-prod:
	@echo "$(txtblu)Compressing uploads ... $(regclr)"
	@ssh $(PROD_HOST) "cd $(PROD_ROOT_FOLDER)wp-content && tar -czf uploads.tar.gz uploads"
	@echo "$(txtblu)Downloading uploads ... $(regclr)"
	@scp -r $(PROD_HOST):$(PROD_ROOT_FOLDER)wp-content/uploads.tar.gz $(BASE_FOLDER)$(APP)/src/wp-content
	@ssh $(PROD_HOST) "cd $(PROD_ROOT_FOLDER)wp-content && rm uploads.tar.gz"
	docker exec $(APP)_wordpress_1 sh -c 'cd /var/www/html/wp-content && tar -xzf uploads.tar.gz uploads && sleep 3 && rm uploads.tar.gz'
	@echo "$(txtgrn)Uploads downloaded successfully from prod ! $(regclr)"

download-wp-config-from-test:
	@scp $(TEST_HOST):/var/www/$(APP)/wp-config.php $(BASE_FOLDER)$(APP)
	@sed -n '96,200p' $(BASE_FOLDER)$(APP)/wp-config.php >> $(BASE_FOLDER)$(APP)/src/wp-config.php
	@rm $(BASE_FOLDER)$(APP)/wp-config.php

download-wp-config-from-prod:
	@scp $(PROD_HOST):$(PROD_ROOT_FOLDER)wp-config.php $(BASE_FOLDER)$(APP)
	@sed -n '96,200p' $(BASE_FOLDER)$(APP)/wp-config.php >> $(BASE_FOLDER)$(APP)/src/wp-config.php
	@rm $(BASE_FOLDER)$(APP)/wp-config.php

