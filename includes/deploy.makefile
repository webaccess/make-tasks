setup-ssh:
	@cat ~/.ssh/id_rsa.pub | ssh $(PROD_HOST) 'mkdir ~/.ssh && cat >> ~/.ssh/authorized_keys'
	@echo "$(txtgrn)SSH key added successfully !$(regclr)"

deploy-theme-to-test:
	@echo "$(txtblu)Deploying the theme to test ... $(regclr)"
	rsync -avz --exclude-from=$(BASE_FOLDER)$(APP)/.deploy $(BASE_FOLDER)$(APP)/src/wp-content/themes/$(APP) $(TEST_HOST):$(TEST_ROOT_FOLDER)$(APP)/wp-content/themes/
	@echo "$(txtgrn)Theme deployed successfully !$(regclr)"

deploy-theme-to-prod:
	@echo "$(txtblu)Deploying the theme to prod ... $(regclr)"
	rsync -avz --exclude-from=$(BASE_FOLDER)$(APP)/.deploy $(BASE_FOLDER)$(APP)/src/wp-content/themes/$(APP) $(PROD_HOST):$(PROD_ROOT_FOLDER)/wp-content/themes/
	@echo "$(txtgrn)Theme deployed successfully !$(regclr)"

deploy-site-to-test:
	@echo "$(txtblu)Deploying the wholesite to test ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP) && tar -czf $(APP).tar.gz src
	@scp $(BASE_FOLDER)$(APP)/$(APP).tar.gz $(TEST_HOST):$(TEST_ROOT_FOLDER)
	@ssh $(TEST_HOST) "cd $(TEST_ROOT_FOLDER) && tar -xzf $(APP).tar.gz src && mv src $(APP)"
	@ssh $(TEST_HOST) "rm $(TEST_ROOT_FOLDER)$(APP).tar.gz"
	@ssh $(TEST_HOST) "chown -R www-data:www-data $(TEST_ROOT_FOLDER)$(APP)"
	@ssh $(TEST_HOST) "chmod -R 777 $(TEST_ROOT_FOLDER)$(APP)/wp-content"
	@rm $(BASE_FOLDER)$(APP)/$(APP).tar.gz
	@echo "$(txtgrn)Site deployed successfully !$(regclr)"

deploy-site-to-prod:
	@echo "$(txtblu)Deploying the wholesite to prod ... $(regclr)"
	@cd $(BASE_FOLDER)$(APP) && tar -czf $(APP).tar.gz src
	@scp $(BASE_FOLDER)$(APP)/$(APP).tar.gz $(PROD_HOST):$(PROD_ROOT_FOLDER)
	@ssh $(PROD_HOST) "cd $(PROD_ROOT_FOLDER) && tar -xzf $(APP).tar.gz src && mv src $(APP)"
	@ssh $(PROD_HOST) "rm $(PROD_ROOT_FOLDER)$(APP).tar.gz"
	@rm $(BASE_FOLDER)$(APP)/$(APP).tar.gz
	@echo "$(txtgrn)Site deployed successfully !$(regclr)"