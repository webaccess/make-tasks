BASE_FOLDER=/Users/lgandelin/Code/

include includes/*

list:
	@echo "------------------------------------------------------------------------------------------------------------------------------"
	@echo ". make $(bldgrn)deploy-theme-to-test$(regclr) $(txtylw)APP=$(regclr)              Déploie le thème Wordpress sur l'environnement de test"
	@echo ". make $(bldpur)deploy-theme-to-prod$(regclr) $(txtylw)APP=$(regclr)              Déploie le thème Wordpress sur l'environnement de production"
	@echo ""
	@echo ". make $(bldgrn)deploy-site-to-test$(regclr) $(txtylw)APP=$(regclr)               Déploie tout le site Wordpress sur l'environnement de test"
	@echo ". make $(bldpur)deploy-site-to-prod$(regclr) $(txtylw)APP=$(regclr)               Déploie tout le site Wordpress sur l'environnement de production"
	@echo "------------------------------------------------------------------------------------------------------------------------------"
	@echo ". make $(bldwht)create-env$(regclr) $(txtylw)APP=$(regclr) $(txtylw)PORT=$(regclr)	 	      Crée un dossier pour un nouvel environnement"
	@echo ". make $(bldwht)init-dev$(regclr) $(txtylw)APP=$(regclr)                          Initialise un nouvel environnement de développement"
	@echo ". make $(bldgrn)init-dev-from-test$(regclr) $(txtylw)APP=$(regclr) 		      Initialise un environnement de développement à partir d'un existant en test "
	@echo ". make $(bldpur)init-dev-from-prod$(regclr) $(txtylw)APP=$(regclr) 		      Initialise un environnement de développement à partir d'un existant en production "
	@echo "------------------------------------------------------------------------------------------------------------------------------"
	@echo ". make $(txtred)delete-env$(regclr) $(txtylw)APP=$(regclr)			      Supprime un environnement de développement"
	@echo "------------------------------------------------------------------------------------------------------------------------------"

create-env:
	@echo "$(txtblu)Creating env folder ... $(regclr)"
	@mkdir $(BASE_FOLDER)$(APP)
	@cp $(BASE_FOLDER)tasks/models/docker-compose.yml $(BASE_FOLDER)$(APP)
	@cp $(BASE_FOLDER)tasks/models/config.makefile $(BASE_FOLDER)$(APP)
	@cp $(BASE_FOLDER)tasks/models/.deploy $(BASE_FOLDER)$(APP)
	@cd $(BASE_FOLDER)$(APP)
	@sed -i '' 's/#PORT#/$(PORT)/g' $(BASE_FOLDER)$(APP)/docker-compose.yml
	@sed -i '' 's/#APP#/$(APP)/g' $(BASE_FOLDER)$(APP)/docker-compose.yml
	@sed -i '' 's/#PORT#/$(PORT)/g' $(BASE_FOLDER)$(APP)/config.makefile
	@echo "$(txtgrn)Env folder created successfully !$(regclr)"